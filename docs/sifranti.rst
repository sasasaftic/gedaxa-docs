========
ŠIFRANTI
========

Šifranti so edinstveni objekti v sistemu Gedaxa, ki so zapisani v obliki številk (šifer), ki predstavljajo vse
predmete, ki jih uporabljamo v naši proizvodnji, od materialov do izdelkov, od strojev do embalaže ter storitev.

Razdeljeni so ni tri dele (podmenije): *Material* (kamor spadajo izdelki, polizdelki, ter surov matrial),
*Ostalo* (orodja, stroji, kontrolna sredstva, pripomočki, pisarniška oprema, embalaža) ter *Storitve*. Za lažjo
prepoznavnost šifrantov, se ti na podlagi delitve začenjajo z različno številko (številke 1-11 predstavljene pred piko).
Številka za piko pa je samodejna naraščujoča, ki se poveča za vsak dodan nov izdelek (s tem sistem zagotavlja
unikatnost šifer). Poleg tega lahko šifrantu določimo tudi interno šifro (prvo število po piki) ter zunanjo šifro.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_1.png


***********************
Kako dodamo nov šifrant
***********************

V meniju *Šifranti* imamo vpogled do vseh naših šifrantov. Tu ji lahko tudi urejamo in dodajamo.

S pritiskom na gumb *Dodaj šifro* se odpre obrazec za vnos nove šifre. *Ime*, *Naziv 1* in *Naziv 2* so poljubna
poimenovanja za šifrant, ki ga želimo vnesti. Iz padajočega menija *Vrsta šifranta* izbiramo med možnostmi
*Izdelek*, *Polizdelek*, *Material*, *Orodja*, *Stroj*, *Kontrolna sredstva*, *Pripomočki*, *Pisarniška oprema*,
*Embalaža* in *Storitev*. Vsaka vrsta šifranta ima svojo pripadajočo številko, ki se prikaže v končni šifri kot prva
števka šifranta (številka pred piko). Posamezne vrste šifranta in njim pripadajoče številke so napisane na desni strani
pod tabelo.

Za lažjo prepoznavnost šifrantov določimo interno šifro, tj. eno števko od 0 do 9, ki bo v končni šifri dodana za piko.
Ostala števila so dodana avtomatično (s tem zagotovimo unikatnost vseh šifer). Seznam internih šifer si lahko
zabeležimo v prazno polje levo pod tabelo (in *Shranimo*), da nam je na razpolago pri prihodnjih vnosih šifrantov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_intern_numbers.png


V naslednjem koraku vpišemo koliko kilogramov, metrov, litrov in enot predstavlja 1 kos izbrnega šifranta. Ta
zapis razmerja med enotami je kasneje uporabljen pri preračunavanju uporabe šifrantov v procesih podjetja.  Količina
kilogram je obvezno polje, ki ga moramo izpolniti. Enote morajo biti celoštevilske saj le s tem sistem zagotavlja
pravilno računanje pretvarjanja enot. Za lažje račuanje enot (v primeru da nimamo celoštevilskih faktorjev) je na voljo
tudi računalo, ki po vnosu samodejno zapiše razmerja v obrazec.

Polje *Opis* je poljubno polje, v katerega lahko zapišemo opombo na šifrant. Po pritisku gumba *Shrani* se šifrant
zabeleži v tabeli *Seznam šifrantov*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_add_1.png

Ko je šifrant vnesen, ga lahko vedno *Uredimo* |pencil| ali *Zbrišemo* |delete| s pritiskom na
ikono v zadnjem stolpcu tabele *Seznam šifrantov*. Po *Tabeli šifrantov* lahko filtriramo po imenu ali (delni) šifri
šifrantov z uporabo funkcije *Filtriraj tabelo*, ki jo najdemo desno nad tabelo. Filtriranje je uporabno za iskanje
samo določene vrste šifrantov, npr. samo strojev, z vpisom iskalnega niza “5.”.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_filter.png

.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>

.. |icon_files| raw:: html

   <i class="icon-files"></i>

.. |icon_updown| raw:: html

   <i class="icon-swap-vertical"></i>
