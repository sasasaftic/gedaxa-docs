===================
ČASOVNICE PROJEKTOV
===================

***************************
Kaj so časovnice projektov?
***************************

Časovnice projektov (*Vodenje projektov > Časovnice projektov*) so grafični prikaz, kako izvajamo
naročnikovo naročilo od začetka do konca.
S klikom na gumb **Opravila** časovnice, vidimo podrobnosti in stanje posameznih korakov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/casovnice/list.png

Časovnica projektov nas vodi skozi potrebne korake procesa izvajanja naročila,
da lahko iz enega mesta izvedemo celotno naročilo v Gedaxi.
Brez iskanja funkcionalnosti ali nepotrebnega brskanja med moduli.

Časovnica je razdeljena na 8 sklopov: obdelava povpraševanja, ponudba, naročilo, delovni nalog in delni delovni nalog, proizvodnja, postopki po proizvodnji in zaključek projekta.
Vsak sklop je v svojem krogu.
Debela modra črta okoli kroga nam sporoči, kateri sklop si trenutno ogledujemo.
Barva kroga in ikone pa povedo, če smo ta sklop že opravili.
Črna barva kaže še ne obdelan korak proizvodnje, medtem ko se opravljeni sklopi obarvajo modro.
Tako vizualno takoj vidimo, na kateri stopnji realizacije je projekt.

Pod vsakim sklopom časovnice je gumb Opravila.
S klikom nanj se odpre seznam vseh opravil, ki jih moramo narediti, da realiziramo določen sklop projekta.
Opravila obarvana z modro nas vodijo do podstrani Gedaxe, kjer lahko izpolnemo opravilo.
Ko odkljukamo vsa opravila znotraj sklopa, se sklop obarva modro, ker je zaključen.


**************************************
Kako ustvarim novo časovnico projekta?
**************************************

Časovnica se ustvari samodejno, ko ustvarimo nov projekt. Da bi ustvarili nov projekt, glej "Kako ustvarim nov projekt"?

*******************************************************************
Kako pogledam odprte, zaključene in preklicane časovnice projektov?
*******************************************************************

Časovnice projektov lahko filtriramo po statusu projekta. Izbiramo lahko med prikazom vseh projektov, odprtih (tj. v teku),
(uspešno) zaključenih ali preklicanih projektov.
Na strani časovnic projektov (*Vodenje projektov > Časovnice projektov*) najdemo zgoraj desno gumb za filtriranje.
Ko kliknemo nanj se odprejo možnosti filtriranja in izberemo poljuben pogled.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/casovnice/filter.png

*********************************
Kako uporabim časovnico projekta?
*********************************
Preprosto odpremo prvi sklop znotraj časovnice in postopa opravimo opravila, ki so določena znotraj tega sklopa, nato
pa preidemo na naslednji sklop.
Vsako opravilo ima dodatno pomoč na podstrani kamor vodi, da ga čim hitreje in uspešno zaključimo.

*********************************
Kako uporabim časovnico projekta?
*********************************
Časovnice projektov pomagajo izvesti naročila hitreje, preprosteje in bolj učinkovito.
Odlikujejo jih 4 glavne prednosti:
1. Vse na enem mestu. Znotraj časovnice najdemo vse korake, ki so potrebni za izvedbo naročila. Ne rabimo brskati ali
preklapljati med različnimi moduli.
2. Vizualno jasna stopnja realizacije. Že na prvi pogled lahko vidimo, na kateri stopnji realizacije se nahaja projekt.
Modre ikone in krogi označujejo že opravljene korake, medtem ko črni označujejo korake, ki jih šele moramo opraviti.
Hkrati vidimo znotraj sklopa, katera opravila so bila odkljukana (tj. opravljena), da tudi znotraj sklopa vemo, do katere stopnje je korak bil izpolnjen.
3. Manj izpuščenih korakov. Z uvedbo seznamom opravil za odkljukat se zmanjša možnost preskakovanja korakov v proizvodnji in se zgodi manj napak.
4. Jasna odgovornost. V seznamu opravil se zabeleži, kdo je odkljukal opravilo in ga označil kot opravljenega. Če se proizvodnja zaplete,
takoj vemo, do koga stopiti v stik za razjasnitev.
