=======
OSTANKI
=======

********************************************************************************************
Kam se shranijo ostanki materialov, ki nastanejo pri ciklih, in kako jih dodamo v skladišče?
********************************************************************************************

Vsi ostanki materialov, ki so nastali v primerih, ko smo ob kreiranju cikla nastavili ostanek, so prikazani v podmeniju
Ostanki, ki se nahaja v modulu Skladiščenje. Za vsak ostanek je na seznamu prikazana količina proizvedenega materiala,
ter ostanek (v procentih), ki smo ga definirali ob kreiranju cikla, hkrati pa na podlagi tega vidimo tudi preračunano
količino ostanka. Ostanki se v skladišče ne dodajo samodejo, pač pa lahko izberemo ostanke, ki naj se dodajo v
skladišče. Za dodajanje ostanka v skladišče za izbran ostanek kliknemo ikono Dodaj v skladišče. Odpre se nam obrazec,
v katerem se za količino izbere preračunana vrednost glede na definiran procent ostanka ter proizvedene količine.
Potrebno je izbrati skladišče, nato pa ostanek dodamo s klikom na gumb Potrdi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/ostanki.png

V primeru, da želimo ostanek dodati v skladišče kot drug material ali spremeniti količino, ki se izračuna samodejno,
lahko to storimo v samem obrazcu - izberemo željeni material in količino.
