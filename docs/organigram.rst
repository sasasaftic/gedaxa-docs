==========
ORGANIGRAM
==========

Organigram je grafična predstavitev zaposlenih in njihovih medsebojnih odnosov v obliki hierarhije, ki se razprostira
od vrha (vodstva) navzdol po oddelkih in zaposlenih znotraj oddelka.
Zgradimo ga od zgoraj navzdol. S klikom na gumb Organigram se odprejo tri možnosti: lahko Dodamo zaposlenega,
Dodamo skupino, ali Uredimo skupino. S klikom na gumb Dodaj zaposlenega lahko izberemo osebo, ki zaseda vodilni položaj
(npr. direktor). S klikom na Dodaj skupino lahko dodamo skupino, npr. računovodstvo.
Ko kliknemo na novo nastali gumb “računovodstvo” lahko izberemo možnost Dodaj zaposlenega in iz padajočega menija
zaposlenih izberemo osebo, ki jo želimo vključiti v izbrano skupino. Vsako odločitev lahko Prekličemo ali Shranimo
med vpisovanjem. Vedno pa lahko spreminjamo in urejamo organigram kasneje, s klikom na gumb, ki ga želimo urediti
(npr. dodamo skupino, zaposlenega, skupino odstranimo ali preimenujemo).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/organigram.png

Pozor: če boste odstranili skupino, boste istočasno odstranili vse veje (skupin in zaposlenih) pod njo.
